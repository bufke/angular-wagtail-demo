import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IWagtailPageDetail } from 'angular-wagtail';

interface IHomeDetails extends IWagtailPageDetail {
  extra_field: string;
}

@Component({
  selector: 'app-home',
  template: `
    <p>Home Works!</p>
    <p>{{ (cmsData$ | async).title }}</p>
  `,
})
export class HomeComponent implements OnInit {
  public cmsData$: Observable<IHomeDetails>;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.cmsData$ = this.route.data.pipe(map(dat => dat.cmsData));
  }
}
