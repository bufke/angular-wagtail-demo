import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetPageDataResolverService } from 'angular-wagtail';
import { HomeComponent } from './home.component';


const routes: Routes = [{
  path: '',
  component: HomeComponent,
  resolve: {
    cmsData: GetPageDataResolverService,
  },
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
