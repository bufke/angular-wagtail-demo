import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { WagtailModule } from 'angular-wagtail';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
  ],
  entryComponents: [NotFoundComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    WagtailModule.forRoot({
      pageTypes: [
        {
          type: 'sandbox.BarPage',
          loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
        },
        {
          type: 'sandbox.FooPage',
          loadChildren: () => import('./foo/foo.module').then(m => m.FooModule)
        },
      ],
      wagtailSiteDomain: 'http://localhost:8000',
      wagtailSiteId: 2,
      notFoundComponent: NotFoundComponent
    }),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
